public class Rechner implements IMyMath {

	@Override
	public int fakultaet(int zahl) {
		if(zahl <= 0) return 1;
		return zahl * fakultaet(zahl - 1);
	}

	@Override
	public int fibonacci(int zahl) {
		if(zahl <= 0) return 0;
		if(zahl <= 2)
			return 1;
		return fibonacci(zahl - 1) + fibonacci(zahl - 2);
	}

}
