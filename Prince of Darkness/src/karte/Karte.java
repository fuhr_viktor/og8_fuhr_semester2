package karte;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class Karte extends Held{
	static Held held = new Held();
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Karte frame = new Karte();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Karte() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblName = new JLabel(held.getName());
		panel.add(lblName);
		
		JLabel lblTyp = new JLabel(held.getTyp());
		lblTyp.setHorizontalAlignment(SwingConstants.TRAILING);
		panel.add(lblTyp);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JLabel lblBild = new JLabel();
		lblBild.setIcon(new ImageIcon("./src/Bildname.png"));
		panel_1.add(lblBild);
		
		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3, BorderLayout.SOUTH);
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2);
		panel_2.setLayout(new GridLayout(4, 0, 0, 0));
		
		JLabel lblLeben = new JLabel("Leben: " + held.getAktLeben());
		panel_2.add(lblLeben);
		
		JLabel lblR�stung = new JLabel("R�stung :" + held.getRuestung());
		panel_2.add(lblR�stung);
		
		JLabel lblMr = new JLabel("Magieresistenz: " + held.getMagieresistenz());
		panel_2.add(lblMr);
		
		JLabel lblAngriff = new JLabel("Angriff: " + held.getAngriff());
		panel_2.add(lblAngriff);

		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5);
		
	}
	
	public Karte(Held held) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblName = new JLabel(held.getName());
		panel.add(lblName);
		
		JLabel lblTyp = new JLabel(held.getTyp());
		lblTyp.setHorizontalAlignment(SwingConstants.TRAILING);
		panel.add(lblTyp);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		
		JLabel lblBild = new JLabel();
		lblBild.setIcon(new ImageIcon("./src/Bildname.png"));
		panel_1.add(lblBild);
		
		JPanel panel_3 = new JPanel();
		contentPane.add(panel_3, BorderLayout.SOUTH);
		panel_3.setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_3.add(panel_4);
		
		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2);
		panel_2.setLayout(new GridLayout(4, 0, 0, 0));
		
		JLabel lblLeben = new JLabel("Leben: " + held.getAktLeben());
		panel_2.add(lblLeben);
		
		JLabel lblR�stung = new JLabel("R�stung :" + held.getRuestung());
		panel_2.add(lblR�stung);
		
		JLabel lblMr = new JLabel("Magieresistenz: " + held.getMagieresistenz());
		panel_2.add(lblMr);
		
		JLabel lblAngriff = new JLabel("Angriff: " + held.getAngriff());
		panel_2.add(lblAngriff);

		JPanel panel_5 = new JPanel();
		panel_3.add(panel_5);
		
	}

	public static Held getHeld() {
		return held;
	}

	public static void setHeld(Held held) {
		Karte.held = held;
	}

}
