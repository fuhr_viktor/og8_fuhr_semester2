package model;

public class Punkt {
	int x;
	int y;
	
	public Punkt() {
		x = 0;
		y = 0;
	}

	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean equals(Punkt p) {
		return(x == p.getX() && y == p.getY());
	}
	
	@Override
	public String toString() {
		return "Punkt [x=" + x + ", y=" + y + "]";
	}

	
}
