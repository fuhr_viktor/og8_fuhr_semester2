package model;

import java.util.Random;

import model.Punkt;

public class Rechteck {
	Punkt p = new Punkt();
	int breite;
	int hoehe;

	public Rechteck() {
		p.setX(0);
		p.setY(0);
		breite = 0;
		hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) { 
		this.p.setX(x);
		this.p.setY(y);
		this.breite = Math.abs(breite);
		this.hoehe = Math.abs(hoehe);
	}
	
	public int positivierer(int wert) {
		if(wert < 0) return wert * -1; //mit mehr Hinterl�stigkeit k�nnte man hier auch Math.abs(wert) zur�ck geben
		else return wert;
	}
	
	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = positivierer(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = positivierer(hoehe);
	}

	public boolean enthaelt(int x, int y) {
		return (this.getX() <= x && (this.getX() + this.getBreite()) >= x && (this.getY() <= y && (this.getY() + this.getHoehe()) >= y));
		
	}
	
	public boolean enthaelt(Punkt p) {
		int tempx = p.getX();
		int tempy = p.getY();
		return this.enthaelt(tempx,tempy);
	}
	
	public void setAll(int x, int y, int breite, int hoehe) {
		p.setX(x);
		p.setY(y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}
	
	public int[] getAll() {
		int[] werte = {p.getX(),p.getY(),breite,hoehe};
		return werte;
	}
	
	public boolean enthaelt(Rechteck rechteck) {
		Punkt urpunkt = new Punkt(rechteck.getX(), rechteck.getY());
		Punkt gegenpunkt = new Punkt(rechteck.getX() + rechteck.getBreite(), rechteck.getY() + rechteck.getHoehe());
		return(this.enthaelt(urpunkt) && this.enthaelt(gegenpunkt));
		//return (this.getX() <= rechteck.getX() && (this.getX() + this.getBreite()) >= (rechteck.getX() + rechteck.getBreite())) && (this.getY() <= rechteck.getY() && (this.getY() + this.getHoehe()) >= (rechteck.getY() + rechteck.getHoehe()));
	}
	
	public static Rechteck generiereZufallsRechteck() {
		Random rdm = new Random();
		int breite = rdm.nextInt(1200);
		int hoehe = rdm.nextInt(1000);
		int x = rdm.nextInt(1200 - breite);
		int y = rdm.nextInt(1000 - hoehe);
		Rechteck zufallsRechteck = new Rechteck(x,y,breite,hoehe);
		return zufallsRechteck;
	}
	
	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}
	
}
