package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;
import test.RechteckViewTest;

import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	BunteRechteckeController brc = new BunteRechteckeController();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(5, 0, 0, 0));
		
		JLabel lblX = new JLabel("x:");
		contentPane.add(lblX);
		
		JTextField tfX = new JTextField();
		tfX.setColumns(10);
		contentPane.add(tfX);
		
		JLabel lblY = new JLabel("y:");
		contentPane.add(lblY);
		
		JTextField tfY = new JTextField();
		tfY.setColumns(10);
		contentPane.add(tfY);
		
		JLabel lblBreite = new JLabel("breite:");
		contentPane.add(lblBreite);
		
		JTextField tfBreite = new JTextField();
		tfBreite.setColumns(10);
		contentPane.add(tfBreite);
		
		JLabel lblHoehe = new JLabel("hoehe:");
		contentPane.add(lblHoehe);
		
		JTextField tfHoehe = new JTextField();
		tfHoehe.setColumns(10);
		contentPane.add(tfHoehe);
		
		JButton btnOkay = new JButton("Okay");
		btnOkay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					int x = Integer.parseInt(tfX.getText());
					int y = Integer.parseInt(tfY.getText());
					int breite = Integer.parseInt(tfBreite.getText());
					int hoehe = Integer.parseInt(tfHoehe.getText());
				
					Rechteck rechteck = new Rechteck(x, y, breite, hoehe);
					brc.add(rechteck);
				
					tfX.setText("");
					tfY.setText("");
					tfBreite.setText("");
					tfHoehe.setText("");
					JOptionPane.showMessageDialog(null, "Rechteck erstellt", "Hier k�nnte ihre Werbung stehen!!!", JOptionPane.OK_OPTION);
				} catch (Exception e2){
					JOptionPane.showMessageDialog(null, "Bitte f�llen Sie alle Felder aus, indem Sie auschlie�lich Zahlen verwenden!!!", "Fehler", JOptionPane.OK_OPTION);
				}
				
				
			}
		});
		contentPane.add(btnOkay);
		
		JButton btnAnzeigen = new JButton("Anzeigen");
		btnAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setTitle("RechteckViewTest");
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				setBounds(0, 0, 1260, 1000);
				contentPane = new Zeichenflaeche(brc);
				contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				setContentPane(contentPane);
				contentPane.setVisible(true);
				
			}
		});
		contentPane.add(btnAnzeigen);
	}

}
