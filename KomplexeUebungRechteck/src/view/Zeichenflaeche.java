package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel{
	private BunteRechteckeController cntl = new BunteRechteckeController();
	
	public Zeichenflaeche(BunteRechteckeController cntl) {
		this.cntl = cntl;
	}
	
@Override
public void paintComponent(Graphics g) {
	for(int i = 0;i < cntl.getRechtecke().size(); i++) {
		g.setColor(Color.black);
		int y = cntl.getRechteck(i).getY();
		int x = cntl.getRechteck(i).getX();
		int breite = cntl.getRechteck(i).getBreite();
		int hoehe = cntl.getRechteck(i).getHoehe();
		g.drawRect(x,y,breite,hoehe);
	 }
	
	}	

}
