package controller;


import java.util.LinkedList;

import model.Rechteck;

public class BunteRechteckeController {
	LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();
	
	public BunteRechteckeController() {
		this.rechtecke.clear();		
	}

	public void add(Rechteck rechteck) {
		rechtecke.add(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}
	
	public Rechteck getRechteck(int pos) { 
		return rechtecke.get(pos);
	}

	public LinkedList<Rechteck> getRechtecke() {
		return rechtecke;
	}
	
	public void generiereZufallsRechtecke(int anzahl){
		reset();
		for(int i = 0; i < anzahl; i++) add(Rechteck.generiereZufallsRechteck());
	}
	
	@Override
	public String toString() {
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}

}
