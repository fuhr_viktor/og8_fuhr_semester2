package test;

import controller.BunteRechteckeController;
import model.Punkt;
import model.Rechteck;

public class RechteckTest {

	public static boolean rechteckeTesten(){
		BunteRechteckeController brc = new BunteRechteckeController();
		Rechteck rand = new Rechteck(0,0,1200,1000);
		boolean next = true;
			for(int i = 0; i < 50_000 && next == true; i++) {
				Rechteck temp = Rechteck.generiereZufallsRechteck();
				brc.add(temp);
				next = rand.enthaelt(temp);
			}	
		return next;
	}
	
	public static void main(String[] args) {
		
		Rechteck r00 = new Rechteck(10,10,30,40);
		Rechteck r01 = new Rechteck(25,25,100,20);
		Rechteck r02 = new Rechteck(260,10,200,100);
		Rechteck r03 = new Rechteck(5,500,300,25);
		Rechteck r04 = new Rechteck(100,100,100,100);
		Rechteck r05 = new Rechteck();
		Rechteck r06 = new Rechteck();
		Rechteck r07 = new Rechteck();
		Rechteck r08 = new Rechteck();
		Rechteck r09 = new Rechteck();

		r05.setAll(200,200,200,200);
		r06.setAll(800,400,20,20);
		r07.setAll(800,450,20,20);
		r08.setAll(850,400,20,20);
		r09.setAll(855,455,25,25);
		
		BunteRechteckeController ctr = new BunteRechteckeController();
		ctr.add(r00);
		ctr.add(r01);
		ctr.add(r02);
		ctr.add(r03);
		ctr.add(r04);
		ctr.add(r05);
		ctr.add(r06);
		ctr.add(r07);
		ctr.add(r08);
		ctr.add(r09);
		
		System.out.println(ctr.toString().equals("BunteRechteckeController [rechtecke=[Rechteck [x=10, y=10, breite=30, hoehe=40], Rechteck [x=25, y=25, breite=100, hoehe=20], Rechteck [x=260, y=10, breite=200, hoehe=100], Rechteck [x=5, y=500, breite=300, hoehe=25], Rechteck [x=100, y=100, breite=100, hoehe=100], Rechteck [x=200, y=200, breite=200, hoehe=200], Rechteck [x=800, y=400, breite=20, hoehe=20], Rechteck [x=800, y=450, breite=20, hoehe=20], Rechteck [x=850, y=400, breite=20, hoehe=20], Rechteck [x=855, y=455, breite=25, hoehe=25]]]"
				));
		
		System.out.println(r00.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"));
		
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);
	    
	    Punkt p = new Punkt(123,321);
	    Punkt p1 = new Punkt(123,321);
	    System.out.println(p.equals(p1));
	    
	    
	    Rechteck t = new Rechteck(10,20,10,10);
	    System.out.println(t.enthaelt(10,11));
	    System.out.println(t.enthaelt(11,10));
	    System.out.println(t.enthaelt(10,9));
	    System.out.println(t.enthaelt(9,10));
	    
	    System.out.println(rechteckeTesten());
	    
	    System.out.println( );
	    
	    Rechteck r10 = new Rechteck(10,10,10,10);
	    Rechteck r11 = new Rechteck(11,11,8,8);
	    System.out.println(r10.enthaelt(r11));
	}

}
